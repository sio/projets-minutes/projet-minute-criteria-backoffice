GRANT USAGE ON SCHEMA "criteria_backoffice" TO "etudiants-slam";
GRANT SELECT ON TABLE "criteria_backoffice"."proposer" TO "etudiants-slam";
GRANT SELECT ON TABLE "criteria_backoffice"."resto" TO "etudiants-slam";
GRANT SELECT ON TABLE "criteria_backoffice"."type_cuisine" TO "etudiants-slam";
