SET client_encoding = 'UTF8';
DROP SCHEMA "criteria_backoffice" CASCADE;
CREATE SCHEMA "criteria_backoffice";

CREATE TABLE "criteria_backoffice"."proposer" (
  "id_r" integer NOT NULL,
  "id_tc" integer NOT NULL
);
CREATE TABLE "criteria_backoffice"."resto" (
  "id" integer NOT NULL,
  "nom" character varying(255) DEFAULT NULL::character varying,
  "num_adr" character varying(20) DEFAULT NULL::character varying,
  "voie_adr" character varying(255) DEFAULT NULL::character varying,
  "cp" character(5) DEFAULT NULL::"bpchar",
  "ville" character varying(255) DEFAULT NULL::character varying,
  "latitude" double precision,
  "longitude" double precision,
  "desc_resto" "text",
  "horaires" "text"
);
CREATE TABLE "criteria_backoffice"."type_cuisine" (
  "id_tc" integer NOT NULL,
  "libelle" character varying(255) DEFAULT NULL::character varying
);
