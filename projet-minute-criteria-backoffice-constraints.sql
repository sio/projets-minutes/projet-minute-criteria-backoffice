ALTER TABLE ONLY "criteria_backoffice"."proposer"
  ADD CONSTRAINT "pk_proposer" PRIMARY KEY ("id_r", "id_tc");
ALTER TABLE ONLY "criteria_backoffice"."resto"
  ADD CONSTRAINT "pk_resto" PRIMARY KEY ("id");
ALTER TABLE ONLY "criteria_backoffice"."type_cuisine"
  ADD CONSTRAINT "pk_type_cuisine" PRIMARY KEY ("id_tc");

ALTER TABLE ONLY "criteria_backoffice"."proposer"
  ADD CONSTRAINT "proposer_resto_fkey" FOREIGN KEY ("id_r") REFERENCES "criteria_backoffice"."resto"("id");
ALTER TABLE ONLY "criteria_backoffice"."proposer"
  ADD CONSTRAINT "proposer_type_cuisine_fkey" FOREIGN KEY ("id_tc") REFERENCES "criteria_backoffice"."type_cuisine"("id_tc");
